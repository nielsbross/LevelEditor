﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LevelEditor
{
    public partial class HazardWindow : Form
    {
        List<String> directions;
        List<String> hazards;
        MainEditorWindow form;
        public Hazard hazard;
        bool editing;

        public HazardWindow(MainEditorWindow form, Hazard hazard, bool editing)
        {
            this.form = form;
            this.editing = editing;
            this.hazard = hazard;
            
            InitializeComponent();
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            
            directions = new List<String>();
            directions.AddRange(new string[] { "UP", "DOWN", "LEFT", "RIGHT" });
            hazards = new List<string>();
            hazards.AddRange(new string[] { "SpikeShooter", "Moving platform" });
            this.comboBox2.DataSource = directions;
            this.comboBox1.DataSource = hazards;

            if (hazard is SpikeShooter)
            {
                /* Make sure the correct hazard is chosen in the combobox and disable the box,
                 to prevent a casting error. */
                this.comboBox1.SelectedIndex = 0;
                this.comboBox1.Enabled = false;

                SpikeShooter s = (SpikeShooter)hazard;

                switch (s.Direction)
                {
                    case Direction.UP:
                        this.comboBox2.SelectedIndex = 0;
                        break;
                    case Direction.DOWN:
                        this.comboBox2.SelectedIndex = 1;
                        break;
                    case Direction.LEFT:
                        this.comboBox2.SelectedIndex = 2;
                        break;
                    case Direction.RIGHT:
                        this.comboBox2.SelectedIndex = 3;
                        break;
                }

                this.xField.Text = "" + s.PosX;
                this.yField.Text = "" + s.PosY;
                this.timingField.Text = "" + s.Timing;
            }
            else if (hazard is MovingPlatform)
            {
                /* Make sure the correct hazard is chosen in the combobox and disable the box,
                 to prevent a casting error. */
                this.comboBox1.SelectedIndex = 1;
                this.comboBox1.Enabled = false;

                MovingPlatform m = (MovingPlatform)hazard;

                switch (m.Direction)
                {
                    case Direction.UP:
                        this.comboBox2.SelectedIndex = 0;
                        break;
                    case Direction.DOWN:
                        this.comboBox2.SelectedIndex = 1;
                        break;
                    case Direction.LEFT:
                        this.comboBox2.SelectedIndex = 2;
                        break;
                    case Direction.RIGHT:
                        this.comboBox2.SelectedIndex = 3;
                        break;
                }

                this.xField.Text = "" + m.PosX;
                this.yField.Text = "" + m.PosY;
                this.lengthField.Text = "" + m.Length; ;
                this.speedField.Text = "" + m.Speed;
                this.movementField.Text = "" + m.MovementAmount;
            }
        }


        private void saveButton_Click(object sender, EventArgs e)
        {
            if (this.comboBox1.SelectedIndex == 0)
            {
                SpikeShooter s = (SpikeShooter)hazard;
                Direction direction = Direction.UP;
                switch (this.comboBox2.SelectedIndex)
                {
                    case 1:
                        direction = Direction.DOWN;
                        break;
                    case 2:
                        direction = Direction.LEFT;
                        break;
                    case 3:
                        direction = Direction.RIGHT;
                        break;
                }
                s.Direction = direction;
                s.PosX = Int32.Parse(this.xField.Text);
                s.PosY = Int32.Parse(this.yField.Text);
                s.Timing = Int32.Parse(this.timingField.Text);
                if (s.Timing <= 0)
                {
                    s.Timing = 1;
                }
                if (!editing)
                {
                    form.AddHazard(s);
                }
                else
                {
                    form.UpdateHazards();
                }
            }
            if (this.comboBox1.SelectedIndex == 1)
            {

                hazard = new MovingPlatform();
                MovingPlatform m = (MovingPlatform)hazard;
                Direction direction = Direction.UP;
                switch (this.comboBox2.SelectedIndex)
                {
                    case 1:
                        direction = Direction.DOWN;
                        break;
                    case 2:
                        direction = Direction.LEFT;
                        break;
                    case 3:
                        direction = Direction.RIGHT;
                        break;
                }
                m.Direction = direction;
                m.PosX = Int32.Parse(this.xField.Text);
                m.PosY = Int32.Parse(this.yField.Text);
                m.Speed = Int32.Parse(this.speedField.Text);
                m.MovementAmount = Int32.Parse(this.movementField.Text);
                m.Length = Int32.Parse(this.lengthField.Text);
                if (m.Speed <= 0)
                {
                    m.Speed = 1;
                } 
                if (m.MovementAmount <= 0)
                {
                    m.MovementAmount = 1;
                }
                if (m.Length <= 0)
                {
                    m.Length = 1;
                }
                if (!editing)
                {
                    form.AddHazard(m);
                }
                else
                {
                    form.UpdateHazards();
                }
            }
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void HazardWindow_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBox1.SelectedIndex == 1)
            {
                timingField.Enabled = false;
                speedField.Enabled = true;
                movementField.Enabled = true;
                lengthField.Enabled = true;
            }
            else
            {
                timingField.Enabled = true;
                speedField.Enabled = false;
                movementField.Enabled = false;
                lengthField.Enabled = false;
            }
        }

    }
}
