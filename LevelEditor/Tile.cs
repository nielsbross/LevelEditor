﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LevelEditor
{
    public class Tile
    {
        private bool invalidate;
        public bool Invalidate { get { return this.invalidate; } set { this.invalidate = value; } }
        private Color color;
        public Color Color { get { return this.color; } set { this.color = value; } }
        private Image image;
        public Image Image { get { return this.image; } set { this.image = value;  } }

        public Tile(Color color)
        {
            this.color = color;
        }


        public Color getColor()
        {
            return color;
        }

        public void setColor(Color color)
        {
            this.color = color;
        }

    }
}
