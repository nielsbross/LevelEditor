﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelEditor
{
   public class SpikeShooter : Hazard
    {
        Direction direction;

        int posX, posY;
        int timing;

        public SpikeShooter(Direction direction, int posX, int posY, int timing)
        {
            this.direction = direction;
            this.posX = posX;
            this.posY = posY;
            this.timing = timing;
        }
        public Direction Direction
        {
            get
            {
                return this.direction;
            }
            set
            {
                this.direction = value;
            }
        }

        public int PosX
        {
            get
            {
                return this.posX;
            }
            set
            {
                this.posX = value;
            }
        }

        public int PosY
        {
            get
            {
                return this.posY;
            }
            set
            {
                this.posY = value;
            }
        }

        public int Timing
        {
            get
            {
                return this.timing;
            }
            set
            {
                this.timing = value;
            }
        }

        public static Direction FromString(string str)
        {
            Direction dir = Direction.UP;
            if (str.Equals("d")) dir = Direction.DOWN;
            if (str.Equals("l")) dir = Direction.LEFT;
            if (str.Equals("r")) dir = Direction.RIGHT;
            return dir;
        }

        public static string FromDirection(Direction dir)
        {
            string str = "u";
            switch (dir)
            {
                case Direction.DOWN:
                    str = "d";
                    break;
                case Direction.LEFT:
                    str = "l";
                    break;
                case Direction.RIGHT:
                    str = "r";
                    break;
            }
            return str;
        }
        public override string ToString()
        {
            return "SpikeShooter - Direction: " + this.direction + " - Timing: " + this.timing + " sec - x: " + this.posX + " - y: " + posY;
        }
    }
    public enum Direction
    {
        UP, DOWN, LEFT, RIGHT

    }
}
