﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelEditor
{
    class MovingPlatform : Hazard
    {
        Direction direction;
        int length;
        int posX, posY;
        int movementAmount;
        float speed;

        public MovingPlatform() { }

        public MovingPlatform(int length, int posX, int posY, int movementAmount, Direction direction, float speed)
        {
            this.direction = direction;
            this.posX = posX;
            this.posY = posY;
            this.length = length;
            this.speed = speed;
            this.movementAmount = movementAmount;
        }
        public Direction Direction
        {
            get
            {
                return this.direction;
            }
            set
            {
                this.direction = value;
            }
        }

        public int PosX
        {
            get
            {
                return this.posX;
            }
            set
            {
                this.posX = value;
            }
        }

        public int PosY
        {
            get
            {
                return this.posY;
            }
            set
            {
                this.posY = value;
            }
        }

        public int Length
        {
            get
            {
                return this.length;
            }
            set
            {
                this.length = value;
            }
        }

        public int MovementAmount
        {
            get
            {
                return this.movementAmount;
            }
            set
            {
                this.movementAmount = value;
            }
        }

        public float Speed
        {
            get
            {
                return this.speed;
            }
            set
            {
                this.speed = value;
            }
        }

        public static Direction FromString(string str)
        {
            Direction dir = Direction.UP;
            if (str.Equals("d")) dir = Direction.DOWN;
            if (str.Equals("l")) dir = Direction.LEFT;
            if (str.Equals("r")) dir = Direction.RIGHT;
            return dir;
        }

        public static string FromDirection(Direction dir)
        {
            string str = "u";
            switch (dir)
            {
                case Direction.DOWN:
                    str = "d";
                    break;
                case Direction.LEFT:
                    str = "l";
                    break;
                case Direction.RIGHT:
                    str = "r";
                    break;
            }
            return str;
        }
        public override string ToString()
        {
            return "Moving platform - Direction: " + this.direction + " - StartPos: (" + this.PosX + "," + this.posY + ") Speed: " + this.speed + " Movementamount: " + this.movementAmount ;
        }
    }
}
