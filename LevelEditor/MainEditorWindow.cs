﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LevelEditor.Commands;
using System.Diagnostics;

namespace LevelEditor
{
    public partial class MainEditorWindow : Form
    {


        List<DrawLineCommand> DrawLineList = new List<DrawLineCommand>();

        // CommandQueue is accessed on "Undo" actions
        private Stack<ICommand> commandQueue;

        // RedoCommandQueue is accessed on "Redo" actions
        private Stack<ICommand> redoCommandQueue;


        /* Sets the offset for painting the gird:
        *  - Mostly used because of scrolling and default zoom view 
        */
        private float paintOffsetX = 0.0f;
        private float paintOffsetY = 0.0f;

        // Used when drawing a line of tiles to fill out holes
        private int oldMouseX, oldMouseY;

        // Flag to set if grid should be painted
        private bool paintGrid = true;

        // Used for file saving and loading
        private string dir = "";
        private string fileName = "";

        // The current Tile image to be drawn on click and draw
        private Image currentImage;

        // Used to store the textures for the available tile types
        private Dictionary<string, Image> imageMap = new Dictionary<string, Image>();


        // Size of drawn rectangles (more precisely describes zoom level)
        private int rectSize = 15;

        // initial grid size (can be changed dynamically)
        private int gridWidth = 30, gridHeight = 30;

        // Coordinates for setting player position
        private int playerX = 0;
        private int playerY = 0;

        // TODO
        private int endX = 0;
        private int endY = 0;
        
        // Makes sure we have a max and min zoom value so we don't go out of bounds
        private int maxZoom, minZoom;

        // The margin for which auto scrolling keeps on working (relative to size of panel)
        private int origXMargin, origYMargin;

        // Initial drawing offset for grid
        private int initDrawOffsetX, initDrawOffsetY;

        // Initial scrolloffset used for autoscrolling sizes
        private int initScrollOffsetX, initScrollOffsetY;

        // The tile array used for drawing the actual grid (this IS the painting area)
        private Tile[,] tiles;

        // List of hazards currently in the level
        private BindingList<Hazard> hazards;
        
        // Key and mouse booleans used for drawing and executing commands
        private bool mouseDown = false;
        private bool resizeBool = true;
        private bool ctrlPressed = false;
        private bool shiftPressed = false;

        public MainEditorWindow()
        {

            commandQueue = new Stack<ICommand>();
            redoCommandQueue = new Stack<ICommand>();
            hazards = new BindingList<Hazard>();

            // Gets the current directory 
            string dir = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));

            // Set the tile array to the proper grid size
            tiles = new Tile[gridWidth, gridHeight];

            // Add all textures available at the moment
            imageMap.Add("empty", Image.FromFile(dir + "/empty.png"));
            imageMap.Add("grass", Image.FromFile(dir + "/grass.png"));
            imageMap.Add("ground", Image.FromFile(dir + "/ground.png"));
            imageMap.Add("background", Image.FromFile(dir + "/background_sky.png"));


            // Initial tile to be drawn is "Empty"
            currentImage = imageMap["empty"];


            // Initialize tile array
            for (int i = 0; i < tiles.GetLength(0); i++)
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    tiles[i, j] = new Tile(Color.White);
                    tiles[i, j].Image = imageMap["empty"];
                }

            InitializeComponent();
            this.javaTestItem.Click += new EventHandler(this.testJava_Click);
            this.KeyPreview = true;
            this.KeyDown += this.form_KeyDown;
            this.KeyUp += this.form_KeyUp;
            /* All not auto-generated code for UI here */
            this.comboBox1.SelectedIndex = 0;
            this.comboBox2.SelectedIndex = 0;
            this.Text = "LevelEditor";
            this.exitItem.Click += new System.EventHandler(exitItem_Click);

            this.listBox1.DataSource = hazards;

            typeof(System.Windows.Forms.Panel).InvokeMember("DoubleBuffered", System.Reflection.BindingFlags.SetProperty |
               System.Reflection.BindingFlags.Instance |
               System.Reflection.BindingFlags.NonPublic, null, gridPanel, new object[] { true });

            // Set initial values (Sort of magic numbers-ish right now)
            maxZoom = 40;
            minZoom = 5;

            initDrawOffsetX = 600;
            initDrawOffsetY = 80;

            initScrollOffsetX = 50;
            initScrollOffsetY = 50;



            origXMargin = 10 + initDrawOffsetX;
            origYMargin = 10 + initDrawOffsetY;


        }


        private void SaveMap(bool saveAs)
        {
            if (saveAs)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.InitialDirectory = "./";
                saveFileDialog.Filter = "Map Files (*.mp) | *.mp | All files (*.*) | *.*";
                saveFileDialog.FilterIndex = 0;
                saveFileDialog.RestoreDirectory = true;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = saveFileDialog.FileName;
                    Save();
                }
            }
            else
            {
                if (this.Text != "LevelEditor")
                {
                    Save();
                }
                else
                {
                    SaveMap(true);
                }
            }
        }

        private void LoadMap()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            string mapString = "";
            openFileDialog1.InitialDirectory = dir + "/saved_maps";
            openFileDialog1.Filter = "All files (*.*)|*.* | Map file (*.mp)|*.mp";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                hazards.Clear();
                System.IO.StreamReader sr = new
                System.IO.StreamReader(openFileDialog1.FileName);
                fileName = openFileDialog1.FileName;
                mapString = sr.ReadToEnd();

                string[] fileNameArray = openFileDialog1.FileName.Split('\\');
                this.Text = fileNameArray[fileNameArray.Length - 1];
                sr.Close();

                string[] array = mapString.Split('\r');

                int offSet = SetUpHazardsFromLoadedMap(array);
                SetGridFromLoadedMap(offSet, array);
                SetTilesFromLoadedMap(offSet, array);
                
            }

            InvalidateAndUpdate(gridPanel);
        }

        private void SetTilesFromLoadedMap(int offSet, string[] array)
        {
            for (int i = offSet; i < array.Length; i++)
            {
                array[i] = array[i].Replace("\r", "").Replace("\n", "").Replace(" ", "");

                for (int j = 0; j < array[i].Length; j++)
                {
                    if (array[i][j] == '0')
                        tiles[j, i - offSet].Image = imageMap["background"];
                    if (array[i][j] == '1')
                        tiles[j, i - offSet].Image = imageMap["grass"];
                    if (array[i][j] == '2')
                        tiles[j, i - offSet].Image = imageMap["ground"];
                }
            }
        }

        private void SetGridFromLoadedMap(int offSet, string[] array)
        {


            gridWidth = array[offSet].Replace("\r", "").Replace("\n", "").Replace(" ", "").Length;
            gridHeight = array.Length - offSet;

            ResizeGrid();
            switch (gridWidth)
            {
                case 30:
                    this.comboBox2.SelectedIndex = 0;
                    break;
                case 40:
                    this.comboBox2.SelectedIndex = 1;
                    break;
                case 50:
                    this.comboBox2.SelectedIndex = 2;
                    break;
                case 60:
                    this.comboBox2.SelectedIndex = 3;
                    break;
                case 90:
                    this.comboBox2.SelectedIndex = 4;
                    break;
            }
        }

        private int SetUpHazardsFromLoadedMap(string[] array)
        {
            string xString = array[0].Split(' ')[1];
            string yString = array[0].Split(' ')[2];

            playerX = Int32.Parse(xString);
            playerY = Int32.Parse(yString);

            playerPosLabel.Text = "Player start position (" + playerX + "," + playerY + ")";
            int offSet = 1;

            for (int i = 1; i < array.Length; i++)
            {
                if (array[i].Contains("spike"))
                {
                    offSet++;
                    Direction direction = SpikeShooter.FromString(array[i].Split(' ')[1]);
                    int timing = Int32.Parse(array[i].Split(' ')[2]);
                    int x = Int32.Parse(array[i].Split(' ')[3]);
                    int y = Int32.Parse(array[i].Split(' ')[4]);
                    SpikeShooter shooter = new SpikeShooter(direction, x, y, timing);
                    hazards.Add(shooter);
                }
                if (array[i].Contains("mp"))
                {
                    offSet++;
                    Direction direction = SpikeShooter.FromString(array[i].Split(' ')[1]);
                    int length = Int32.Parse(array[i].Split(' ')[2]);
                    int x = Int32.Parse(array[i].Split(' ')[3]);
                    int y = Int32.Parse(array[i].Split(' ')[4]);
                    int movementAmount = Int32.Parse(array[i].Split(' ')[5]);
                    float speed = float.Parse(array[i].Split(' ')[6]);
                    MovingPlatform m = new MovingPlatform(length, x, y, movementAmount, direction, speed);
                    hazards.Add(m);
                }
                if (array[i].Contains("end"))
                {
                    offSet++;
                    endX = Int32.Parse(array[i].Split(' ')[1]);
                    endY = Int32.Parse(array[i].Split(' ')[2]);
                }
            }
            Console.WriteLine("Shooters: " + hazards.Count);
            this.listBox1.DataSource = hazards;

            return offSet;
        }

        private void RefreshGrid()
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    tiles[i, j] = new Tile(Color.White);
                    tiles[i, j].Image = imageMap["empty"];
                }

        }



        private static Tile CreateTileFromColor(Color color)
        {
            return new Tile(color);
        }

        //private static Tile CreateTileFromFile(string fileName)
        //{
        //    return new Tile(fileName);
        //}

        private void PaintTileMap(Graphics gr)
        {
            Pen pen = new Pen(Color.FromArgb(50, 0, 0, 0));

            for (int i = 0; i < tiles.GetLength(0); i++)
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    Point tempPoint = new Point(rectSize * i - (int)paintOffsetX + initDrawOffsetX, rectSize * j - (int)paintOffsetY + initDrawOffsetY);

                    gr.FillRectangle(new SolidBrush(tiles[i, j].getColor()), tempPoint.X, tempPoint.Y, rectSize, rectSize);


                    GraphicsUnit units = GraphicsUnit.Pixel;
                    gr.DrawImage(tiles[i, j].Image, new Rectangle(tempPoint.X, tempPoint.Y, rectSize, rectSize), tiles[i, j].Image.GetBounds(ref units), GraphicsUnit.Pixel);
                    if (paintGrid)
                    {
                        gr.DrawRectangle(pen, new Rectangle(tempPoint, new Size(rectSize, rectSize)));
                    }
                }
        }

        private void PaintHazards(Graphics gr)
        {

            Pen pen2 = new Pen(Color.FromArgb(255, 0, 0, 0));
            Pen pen3 = new Pen(Color.FromArgb(255, 255, 0, 0));
            SolidBrush shooterBrush = new SolidBrush(Color.FromArgb(255, 0, 0, 0));
            SolidBrush mBrush1 = new SolidBrush(Color.FromArgb(255, 0, 255, 0));
            SolidBrush mBrush2 = new SolidBrush(Color.FromArgb(255, 0, 0, 255));
            foreach (Hazard s in hazards)
            {

                if (s is MovingPlatform)
                {
                    MovingPlatform m = (MovingPlatform)s;
                    Direction dir = m.Direction;
                    int startX = m.PosX * rectSize - (int)paintOffsetX + (rectSize / 4) + initDrawOffsetX;
                    int startY = m.PosY * rectSize - (int)paintOffsetY + (rectSize / 4) + initDrawOffsetY;
                    int xEnd = startX;
                    int yEnd = startY;

                    switch (dir)
                    {
                        case Direction.UP:
                            yEnd -= m.MovementAmount * rectSize;
                            break;
                        case Direction.DOWN:
                            yEnd += m.MovementAmount * rectSize;
                            break;
                        case Direction.LEFT:
                            xEnd -= m.MovementAmount * rectSize;
                            break;
                        case Direction.RIGHT:
                            xEnd += m.MovementAmount * rectSize;
                            break;
                    }

                    gr.FillRectangle(mBrush1, new Rectangle(startX, startY, rectSize / 2, rectSize / 2));
                    gr.FillRectangle(mBrush2, new Rectangle(xEnd, yEnd, rectSize / 2, rectSize / 2));
                    gr.DrawLine(pen2, new Point(startX, startY), new Point(xEnd, yEnd));
                }
                if (s is SpikeShooter)
                {
                    SpikeShooter s2 = (SpikeShooter)s;

                    int x = s2.PosX * rectSize - (int)paintOffsetX + (rectSize / 4) + initDrawOffsetX;
                    int y = s2.PosY * rectSize - (int)paintOffsetY + (rectSize / 4) + initDrawOffsetY;
                    gr.FillEllipse(shooterBrush, x, y, rectSize / 2, rectSize / 2);
                    switch (s2.Direction)
                    {
                        case Direction.UP:
                            gr.DrawLine(pen3, new Point(x + rectSize / 4, y), new Point(x + rectSize / 4, y + rectSize / 4));
                            break;
                        case Direction.DOWN:
                            gr.DrawLine(pen3, new Point(x + rectSize / 4, y + rectSize / 4), new Point(x + rectSize / 4, y + rectSize / 2));
                            break;
                        case Direction.LEFT:
                            gr.DrawLine(pen3, new Point(x + rectSize / 4, y + rectSize / 4), new Point(x, y + rectSize / 4));
                            break;
                        case Direction.RIGHT:
                            gr.DrawLine(pen3, new Point(x + rectSize / 4, y + rectSize / 4), new Point(x + rectSize / 2, y + rectSize / 4));
                            break;
                    }
                }
            }
        }

        private void gridPanel_Paint(object sender, PaintEventArgs e)
        {
            Image buffer = new Bitmap((gridWidth + 200) * rectSize, (gridHeight + 200) * (rectSize + 5) + 100);
            Graphics gr = Graphics.FromImage(buffer);


            Pen pen2 = new Pen(Color.FromArgb(255, 0, 0, 0));
            Pen pen3 = new Pen(Color.FromArgb(255, 255, 0, 0));


            PaintTileMap(gr);
         
            int tempX = playerX * rectSize - (int)paintOffsetX + initDrawOffsetX;
            int tempY = playerY * rectSize - (int)paintOffsetY + initDrawOffsetY;
            gr.DrawLine(pen2, new Point(tempX, tempY), new Point(tempX + rectSize, tempY + rectSize));
            gr.DrawLine(pen2, new Point(tempX, tempY + rectSize), new Point(tempX + rectSize, tempY));

            tempX = endX * rectSize - (int)paintOffsetX + initDrawOffsetX;
            tempY = endY * rectSize - (int)paintOffsetY + initDrawOffsetY;
            gr.DrawLine(pen3, new Point(tempX, tempY), new Point(tempX + rectSize, tempY + rectSize));
            gr.DrawLine(pen3, new Point(tempX, tempY + rectSize), new Point(tempX + rectSize, tempY));

            PaintHazards(gr);

            gr.Dispose();
            e.Graphics.DrawImage(buffer, 0, 0);
            buffer.Dispose();

        }


        private void saveItem_Click(object sender, EventArgs e)
        {
            SaveMap(false);
        }

        private void saveAsItem_Click(object sender, EventArgs e)
        {
            SaveMap(true);
            this.savedLabel.Text = "Last saved: " + DateTime.Now.ToString();
        }

        private void loadItem_Click(object sender, EventArgs e)
        {
            LoadMap();
        }


        private void gridPanel_Click(object sender, MouseEventArgs e)
        {
            if (shiftPressed && ctrlPressed)
            {
                endX = (e.X + (int)paintOffsetX - initDrawOffsetX) / rectSize;
                endY = (e.Y + (int)paintOffsetY - initDrawOffsetY) / rectSize;
                endLabel.Text = "Level end position (" + endX + "," + endY + ")";

                InvalidateAndUpdate(gridPanel);
            }
            else if (shiftPressed)
            {
                playerX = (e.X + (int)paintOffsetX - initDrawOffsetX) / rectSize;
                playerY = (e.Y + (int)paintOffsetY - initDrawOffsetY) / rectSize;
                playerPosLabel.Text = "Player start position (" + playerX + "," + playerY + ")";

                InvalidateAndUpdate(gridPanel);
            }
            else
            {
                if (isPointWithinBounds(e.X,e.Y))
                {
                    int x = (e.X + (int)paintOffsetX - initDrawOffsetX) / rectSize;
                    int y = (e.Y + (int)paintOffsetY - initDrawOffsetY) / rectSize;
                    Point tempPoint = new Point(rectSize * x - (int)paintOffsetX + initDrawOffsetX, rectSize * y - (int)paintOffsetY + initDrawOffsetY);

                    ICommand com = new DrawTileCommand(this, x, y, tiles[x, y].Image, currentImage);
                    ExecuteAndQueueCommand(com);
                }
            }
            
        }

        private void DrawTileLine(int oldMouseX, int oldMouseY, int newX, int newY)
        {
            
           
                int x = newX;
                int y = newY;
                if (oldMouseX >= 0 && oldMouseY >= 0)
                {
                    int distanceX = x - oldMouseX;
                    int distanceY = y - oldMouseY;

                    int dx = -1;
                    int dy = -1;

                    if (distanceX < 0) dx = 1;
                    if (distanceY < 0) dy = 1;

                    distanceX = Math.Abs(distanceX);
                    distanceY = Math.Abs(distanceY);

                    int counter = 0;
                    List<DrawTileCommand> tilesToDraw = new List<DrawTileCommand>();
                    while (distanceX > 0 || distanceY > 0)
                    {

                        if (x + dx < tiles.GetLength(0) && x + dx >= 0 && y + dy < tiles.GetLength(1) && y + dy >= 0)
                        {


                            DrawTileCommand com = new DrawTileCommand(this, x, y, tiles[x, y].Image, currentImage);
                            tilesToDraw.Add(com);
                            //tiles[x, y].Image = currentImage;
                            counter++;
                            if (distanceX > 0) distanceX--;
                            if (distanceY > 0) distanceY--;
                            if (distanceX == 0) dx = 0;
                            if (distanceY == 0) dy = 0;
                            x += dx;
                            y += dy;
                        }

                        else
                        {
                            break;
                        }
                    }

                    ICommand dlc = new DrawLineCommand(this, tilesToDraw);
                    DrawLineList.Add(dlc as DrawLineCommand);
                    dlc.Execute();
                    redoCommandQueue.Clear();
                
            }
        }

        

        private void gridPanel_MouseMove(object sender, MouseEventArgs e)
        {

            if (mouseDown)
            {

                int newX = (e.X + (int)paintOffsetX - initDrawOffsetX) / rectSize;
                int newY = (e.Y + (int)paintOffsetY - initDrawOffsetY) / rectSize;

                if (isPointWithinBounds(e.X, e.Y))
                {
                    DrawTileLine(oldMouseX, oldMouseY, newX, newY);
                }
                
                oldMouseX = newX;
                oldMouseY = newY;
                InvalidateAndUpdate(gridPanel);

                }
            
        }

        private void gridPanel_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
        }

        private void gridPanel_MouseUp(object sender, MouseEventArgs e)
        {
            DrawLineCommand tempDlc;
            List<DrawTileCommand> tempTilesToDraw = new List<DrawTileCommand>();

            foreach(DrawLineCommand dlc in DrawLineList) {
                foreach(DrawTileCommand dtc in dlc.TilesDrawn)
                tempTilesToDraw.Add(dtc);
            }

            tempDlc = new DrawLineCommand(this, tempTilesToDraw);
            commandQueue.Push(tempDlc);
            DrawLineList.Clear();

            mouseDown = false;
            oldMouseX = -1;
            oldMouseY = -1;
        }


        private void fillGrid_Click(object sender, EventArgs e)
        {

            // Need temporary but identical array to send
            Tile[,] tempTiles = new Tile[gridWidth, gridHeight];
            for (int i = 0; i < tiles.GetLength(0); i++)
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    tempTiles[i, j] = new Tile(Color.White);
                    tempTiles[i, j].Image = tiles[i, j].Image;
                }


            FillGridCommand fgc = new FillGridCommand(this, tempTiles, currentImage);
            ExecuteAndQueueCommand(fgc);
            InvalidateAndUpdate(gridPanel);
        }


        private void exitItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void form_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.ControlKey:
                    ctrlPressed = true;
                    break;
                case Keys.ShiftKey:
                    shiftPressed = true;
                    break;
                case Keys.S:
                    if (ctrlPressed)
                    {
                        SaveMap(false);
                    }
                    break;
                case Keys.Oemplus:
                    if (ctrlPressed)
                    {
                        ICommand zoomCom = new ZoomInCommand(this);
                        zoomCom.Execute();
                    }
                    break;
                case Keys.OemMinus:
                    if (ctrlPressed)
                    {
                        ICommand zoomOutCom = new ZoomOutCommand(this);
                        zoomOutCom.Execute();
                    }
                    break;
            }
        }
        private void form_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.ControlKey:
                    ctrlPressed = false;
                    break;
                case Keys.ShiftKey:
                    shiftPressed = false;
                    break;
                case Keys.Z:
                    if (ctrlPressed)
                    {
                        if (commandQueue.Count > 0)
                        {
                            ICommand com = commandQueue.Pop();
                            com.Reverse();
                            redoCommandQueue.Push(com);
                            InvalidateAndUpdate(gridPanel);
                        }
                    }
                    break;
                case Keys.Y:
                    if (ctrlPressed)
                    {
                        if (redoCommandQueue.Count > 0)
                        {
                            ICommand com = redoCommandQueue.Pop();
                            com.Execute();
                            commandQueue.Push(com);
                            InvalidateAndUpdate(gridPanel); 
                        }
                    }
                    break;
            }
        }
         private void testJava_Click(object sender, EventArgs e)
         {
             SaveMap(false);
             string dir = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
             //dir = dir.Replace(" ", "^ ");
             System.Diagnostics.Process process = new System.Diagnostics.Process();
             // Redirect the output stream of the child process.
             process.EnableRaisingEvents = false;
             process.StartInfo.UseShellExecute = false;
             process.StartInfo.RedirectStandardOutput = true;
             process.StartInfo.FileName = "java.exe";
             process.StartInfo.Arguments = "-jar \"" + dir + "\\game.jar\"" + " \"" + fileName + "\"";
             Console.WriteLine("Opening: " + process.StartInfo.FileName + " " + process.StartInfo.Arguments);
             // + '"' + fileName.Replace("\\", "/")
             process.Start();
             // Do not wait for the child process to exit before
             // reading to the end of its redirected stream.

             // Read the output stream first and then wait.
             Console.WriteLine(process.StandardOutput.ReadToEnd());
             process.WaitForExit();
             ////process.WaitForExit();
         }

        private void testRunItem_Click(object sender, EventArgs e)
        {
            SaveMap(false);
            string dir = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            ////dir = dir.Replace(" ", "^ ");
            //System.Diagnostics.Process process = new System.Diagnostics.Process();
            //// Redirect the output stream of the child process.
            //process.EnableRaisingEvents = false;
            //process.StartInfo.UseShellExecute = false;
            //process.StartInfo.RedirectStandardOutput = true;
            //process.StartInfo.FileName = "java.exe";
            //process.StartInfo.Arguments = "-jar \"" + dir + "\\game.jar\"" + " \"" + fileName + "\"";
            //Console.WriteLine("Opening: " + process.StartInfo.FileName + " " + process.StartInfo.Arguments);
            //// + '"' + fileName.Replace("\\", "/")
            //process.Start();
            //// Do not wait for the child process to exit before
            //// reading to the end of its redirected stream.

            //// Read the output stream first and then wait.
            //Console.WriteLine(process.StandardOutput.ReadToEnd());
            //process.WaitForExit();
            //////process.WaitForExit();

            //System.Diagnostics.Process process = new System.Diagnostics.Process();
            //// Redirect the output stream of the child process.
            //process.EnableRaisingEvents = false;
            //process.StartInfo.UseShellExecute = false;
            //process.StartInfo.RedirectStandardOutput = true;
            //process.StartInfo.Arguments = "-jar \"" + dir + "\\game.jar\"" + " \"" + fileName + "\"";
            //Console.WriteLine("Opening: " + process.StartInfo.FileName + " " + process.StartInfo.Arguments);
            //// + '"' + fileName.Replace("\\", "/")
          
            //start.FileName = dir + "\\Game\\SideScrollerRPG.exe";
            //process.Start(start);
            //// Do not wait for the child process to exit before
            //// reading to the end of its redirected stream.

            //// Read the output stream first and then wait.
            //Console.WriteLine(process.StandardOutput.ReadToEnd());
            //process.WaitForExit();
            ////process.WaitForExit();

            // Prepare the process to run
            ProcessStartInfo start = new ProcessStartInfo();
            // Enter in the command line arguments, everything you would enter after the executable name itself
            start.Arguments = fileName;
            // Enter the executable to run, including the complete path
            start.FileName = dir + "\\Game\\SideScrollerRPG.exe";
            // Do you want to show a console window?
            start.WindowStyle = ProcessWindowStyle.Normal;
            start.CreateNoWindow = false;
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            start.Verb = "runas";
            int exitCode;


            // Run the external process & wait for it to finish
            using (Process proc = Process.Start(start))
            {
                proc.WaitForExit();

                // Retrieve the app's exit code
                exitCode = proc.ExitCode;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            currentImage = imageMap[(string)comboBox.SelectedItem];
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

            ComboBox comboBox = (ComboBox)sender;
            string selectedItem = (string)comboBox.SelectedItem;

            if (Int32.Parse(selectedItem.Substring(0, 2)) < gridWidth)
            {

                if (MessageBox.Show("Are you sure you want to resize to a smaller size? Any tiles on greater indices will be removed",
                    "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {

                    resizeBool = false;

                }
                else
                {
                    resizeBool = true;
                }

            }

            if (resizeBool)
            {
                switch (selectedItem)
                {
                    case "30x30":
                        gridWidth = 30;
                        gridHeight = 30;
                        break;
                    case "40x30":
                        gridWidth = 40;
                        gridHeight = 30;
                        break;
                    case "50x30":
                        gridWidth = 50;
                        gridHeight = 30;
                        break;
                    case "60x30":
                        gridWidth = 60;
                        gridHeight = 30;
                        //this.Width = 1200;
                        break;
                    case "90x30":
                        gridWidth = 90;
                        gridHeight = 30;
                        break;
                    default:
                        break;
                }

                ResizeGrid();

            }
        }

        private void tile_Click(object sender, EventArgs e)
        {
            PictureBox picBox = (PictureBox)sender;
            string tag = (string)picBox.Tag;
            if (imageMap.ContainsKey(tag))
            {
                currentImage = imageMap[tag];
                selectedTileBox.Text = tag;
            }
        }

        

        private void gridPanel_OnScroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation.Equals(System.Windows.Forms.ScrollOrientation.HorizontalScroll))
            {
                paintOffsetX = e.NewValue;
            }
            else
            {
                paintOffsetY = e.NewValue;
            }

            InvalidateAndUpdate(gridPanel);
        }



        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void form_Closed(object sender, FormClosedEventArgs e)
        {
            closed();
        }

        private void closed()
        {
            DialogResult dialogResult = MessageBox.Show("Do you want to save your map before exiting?", "Saving", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SaveMap(false);
            }
        }
        private void ResizeGrid()
        {
            
            Tile[,] tempTileArr = new Tile[gridWidth, gridHeight];
            Rectangle[,] tempRectArr = new Rectangle[gridWidth, gridHeight];

            for (int i = 0; i < gridWidth; i++)
            {
                for (int j = 0; j < gridHeight; j++)
                {
                    tempTileArr[i, j] = new Tile(Color.White);
                    tempTileArr[i, j].Image = imageMap["empty"];
                }
            }


            for (int i = 0; i < Math.Min(tiles.GetLength(0), tempTileArr.GetLength(0)); i++)
                for (int j = 0; j < Math.Min(tiles.GetLength(1), tempTileArr.GetLength(1)); j++)
                {
                    tempTileArr[i, j].Image = tiles[i, j].Image;
                }

            tiles = tempTileArr;

            //gridPanel.Size = new Size(gridWidth * (rectSize + 5), gridHeight * (rectSize + 5));
            this.gridPanel.AutoScrollMargin = new System.Drawing.Size(origXMargin, origYMargin);
            this.gridPanel.AutoScrollMinSize = new System.Drawing.Size(rectSize * (tiles.GetLength(0) + initScrollOffsetX), (rectSize / 2) * (tiles.GetLength(1) + initScrollOffsetY));
            paintOffsetX = 0F;
            paintOffsetY = 0F;

            // 18000 is based of 30x30 iniDrawOffsetX was 600 and 90x30 was 200 so that gave 18000 / 30 and 90 
            initDrawOffsetX = 18000 / gridWidth;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void gridPanel_onMouseWheel(object sender, MouseEventArgs e)
        {
            paintOffsetY = gridPanel.VerticalScroll.Value;

        }

        private void mainWindow_mouseWheel(object sender, MouseEventArgs e)
        {
            gridPanel.Focus();

        }

       
       

        private void zoom(object sender, EventArgs e)
        {
            string tag = (string)((Button)sender).Tag;


            switch (tag)
            {
                case "zoomIn":
                    if (rectSize <= maxZoom)
                    {
                        ICommand zoomCom = new ZoomInCommand(this);
                        zoomCom.Execute();
                    }
                    break;
                case "zoomOut":
                    if (rectSize >= minZoom)
                    {
                        ICommand zoomOutCom = new ZoomOutCommand(this);
                        zoomOutCom.Execute();
                    }
                    break;
                default:
                    break;
            }


            InvalidateAndUpdate(gridPanel);
        }

        private void setDefaultZoom(object sender, EventArgs e)
        {

            ICommand defZoomCom = new DefaultZoomCommand(this, rectSize);
            defZoomCom.Execute();

            InvalidateAndUpdate(gridPanel);
        }

        private void addHazardButton_Click(object sender, EventArgs e)
        {
            new HazardWindow(this, new NewHazard(), false).Show();
        }

        private void editHazardButton_Click(object sender, EventArgs e)
        {
            new HazardWindow(this, hazards[listBox1.SelectedIndex], true).Show();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if(this.listBox1.SelectedIndex > -1)
                hazards.Remove(hazards[this.listBox1.SelectedIndex]);
        }

        private void endLabel_Click(object sender, EventArgs e)
        {

        }

        private void playerPosLabel_Click(object sender, EventArgs e)
        {

        }

        private void drawGridCheckBox_OnCheckedChanged(object sender, EventArgs e)
        {
            paintGrid = !paintGrid;
            InvalidateAndUpdate(gridPanel);
        }


        private void Save()
        {
            string[] filePathArray = fileName.Split('\\');
            this.Text = "LevelEditor - " + filePathArray[filePathArray.Length - 1];
            string dir = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName))//(dir + "/saved_maps/" + fileName + ".mp"))
            {
                file.WriteLine("playerpos " + playerX + " " + playerY);
                file.WriteLine("end " + endX + " " + endY);
                foreach (Hazard h in hazards)
                {
                    if (h is SpikeShooter)
                    {
                        SpikeShooter s = (SpikeShooter)h;
                        file.WriteLine("spike " + SpikeShooter.FromDirection(s.Direction) + " " + s.Timing + " " + s.PosX + " " + s.PosY);
                    }
                    if (h is MovingPlatform)
                    {
                        MovingPlatform m = (MovingPlatform)h;
                        file.WriteLine("mp " + MovingPlatform.FromDirection(m.Direction) + " " + m.Length + " " + m.PosX + " " + m.PosY + " " + m.MovementAmount + " " + m.Speed);
                    }
                }

                for (int i = 0; i < tiles.GetLength(1); i++)
                {
                    if (i != 0)
                        file.WriteLine();
                    for (int j = 0; j < tiles.GetLength(0); j++)
                    {
                        if (tiles[j, i].Image == imageMap["grass"])
                        {
                            file.Write("1");
                        }
                        else if (tiles[j, i].Image == imageMap["ground"])
                        {
                            file.Write("2");
                        }
                        else
                        {
                            file.Write("0");
                        }
                    }
                }
            }
        }
    }

}
