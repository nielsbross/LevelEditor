﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using LevelEditor.Commands;

namespace LevelEditor
{
    public partial class MainEditorWindow
    {

        private bool isPointWithinBounds(int x, int y)
        {
            return ((x + (int)paintOffsetX - initDrawOffsetX) / rectSize < tiles.GetLength(0) &&
                    (y + (int)paintOffsetY - initDrawOffsetY) / rectSize < tiles.GetLength(1) &&
                    (x + (int)paintOffsetX - initDrawOffsetX) / rectSize > -1 &&
                    (y + (int)paintOffsetY - initDrawOffsetY) / rectSize > -1);
        }


        public void DrawTile(int x, int y, Image image)
        {
            tiles[x, y].Image = image;
            gridPanel.Invalidate();
        }


        private void InvalidateAndUpdate(Panel panel)
        {
            panel.Invalidate(true);
            panel.Update();
        }
       

        private void ExecuteAndQueueCommand(ICommand command)
        {
            command.Execute();
            commandQueue.Push(command);
            redoCommandQueue.Clear();
        }

        public void SetGrid(Tile[,] newGrid)
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    tiles[i, j].Image = newGrid[i, j].Image;
                }
            gridPanel.Invalidate(true);
            gridPanel.Update();
        }

        public void FillGrid(Image newImage)
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    tiles[i, j].Image = newImage;
                }
        }

        public void ZoomIn()
        {
            rectSize++;
            this.gridPanel.AutoScrollMinSize = new System.Drawing.Size(rectSize * (tiles.GetLength(0) + initScrollOffsetX), (rectSize / 2) * (tiles.GetLength(1) + initScrollOffsetY));
            gridPanel.Invalidate(true);
            gridPanel.Update();
        }

        public void ZoomOut()
        {
            rectSize--;
            this.gridPanel.AutoScrollMinSize = new System.Drawing.Size(rectSize * (tiles.GetLength(0) + initScrollOffsetX), (rectSize / 2) * (tiles.GetLength(1) + initScrollOffsetY));
            gridPanel.Invalidate(true);
            gridPanel.Update();
        }

        public void DefaultZoom()
        {
            rectSize = 15;

            this.gridPanel.AutoScrollMargin = new System.Drawing.Size(origXMargin + initDrawOffsetX, origYMargin + initDrawOffsetY);
            this.gridPanel.AutoScrollMinSize = new System.Drawing.Size(rectSize * (tiles.GetLength(0) + initScrollOffsetX), (rectSize / 2) * (tiles.GetLength(1) + initScrollOffsetY));
            paintOffsetX = 0F;
            paintOffsetY = 0F;
        }

        public void SetZoom(int newRectSize)
        {
            this.rectSize = newRectSize;
        }


        public void AddHazard(Hazard hazard)
        {
            hazards.Add(hazard);
            this.gridPanel.Invalidate(true);
            gridPanel.Update();
        }

        public void UpdateHazards()
        {
            this.listBox1.DataSource = null;
            this.listBox1.DataSource = hazards;
            this.gridPanel.Invalidate(true);
            gridPanel.Update();
        }

    }
}
