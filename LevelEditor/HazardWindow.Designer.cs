﻿namespace LevelEditor
{
    partial class HazardWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.xField = new System.Windows.Forms.NumericUpDown();
            this.yField = new System.Windows.Forms.NumericUpDown();
            this.timingField = new System.Windows.Forms.NumericUpDown();
            this.speedLabel = new System.Windows.Forms.Label();
            this.speedField = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.movementField = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.lengthField = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.xField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speedField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.movementField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lengthField)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(26, 38);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 24);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Position";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(174, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Timing";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(174, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Direction";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "x";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "y";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(177, 38);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 24);
            this.comboBox2.TabIndex = 9;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(10, 188);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(101, 38);
            this.saveButton.TabIndex = 11;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(159, 188);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(121, 38);
            this.cancelButton.TabIndex = 12;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // xField
            // 
            this.xField.Location = new System.Drawing.Point(46, 106);
            this.xField.Name = "xField";
            this.xField.Size = new System.Drawing.Size(120, 22);
            this.xField.TabIndex = 13;
            // 
            // yField
            // 
            this.yField.Location = new System.Drawing.Point(46, 136);
            this.yField.Name = "yField";
            this.yField.Size = new System.Drawing.Size(120, 22);
            this.yField.TabIndex = 14;
            // 
            // timingField
            // 
            this.timingField.Location = new System.Drawing.Point(177, 107);
            this.timingField.Name = "timingField";
            this.timingField.Size = new System.Drawing.Size(120, 22);
            this.timingField.TabIndex = 15;
            // 
            // speedLabel
            // 
            this.speedLabel.AutoSize = true;
            this.speedLabel.Location = new System.Drawing.Point(316, 84);
            this.speedLabel.Name = "speedLabel";
            this.speedLabel.Size = new System.Drawing.Size(49, 17);
            this.speedLabel.TabIndex = 16;
            this.speedLabel.Text = "Speed";
            // 
            // speedField
            // 
            this.speedField.Location = new System.Drawing.Point(319, 107);
            this.speedField.Name = "speedField";
            this.speedField.Size = new System.Drawing.Size(120, 22);
            this.speedField.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(319, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "Movement amount";
            // 
            // movementField
            // 
            this.movementField.Location = new System.Drawing.Point(319, 161);
            this.movementField.Name = "movementField";
            this.movementField.Size = new System.Drawing.Size(120, 22);
            this.movementField.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(319, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 17);
            this.label8.TabIndex = 20;
            this.label8.Text = "Length";
            // 
            // lengthField
            // 
            this.lengthField.Location = new System.Drawing.Point(322, 39);
            this.lengthField.Name = "lengthField";
            this.lengthField.Size = new System.Drawing.Size(120, 22);
            this.lengthField.TabIndex = 21;
            // 
            // HazardWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 412);
            this.Controls.Add(this.lengthField);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.movementField);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.speedField);
            this.Controls.Add(this.speedLabel);
            this.Controls.Add(this.timingField);
            this.Controls.Add(this.yField);
            this.Controls.Add(this.xField);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Name = "HazardWindow";
            this.Text = "HazardWindow";
            this.Load += new System.EventHandler(this.HazardWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speedField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.movementField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lengthField)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.NumericUpDown xField;
        private System.Windows.Forms.NumericUpDown yField;
        private System.Windows.Forms.NumericUpDown timingField;
        private System.Windows.Forms.Label speedLabel;
        private System.Windows.Forms.NumericUpDown speedField;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown movementField;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown lengthField;
    }
}