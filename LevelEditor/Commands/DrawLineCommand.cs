﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace LevelEditor.Commands
{
    class DrawLineCommand : ICommand
    {

        MainEditorWindow window;
        List<DrawTileCommand> tilesDrawn;
        public List<DrawTileCommand> TilesDrawn { get { return this.tilesDrawn;} set {this.tilesDrawn = value;}}

        public DrawLineCommand(MainEditorWindow window, List<DrawTileCommand> tilesDrawn)
        {
            this.window = window;
            this.tilesDrawn = tilesDrawn;
        }

        public void Execute()
        {
            foreach (DrawTileCommand dtc in tilesDrawn)
            {
                dtc.Execute();
            }
        }

        public void Reverse()
        {
            foreach (DrawTileCommand dtc in tilesDrawn)
            {
                dtc.Reverse();
            }
        }
    }
}