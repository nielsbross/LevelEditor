﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelEditor.Commands
{
    class DefaultZoomCommand : ICommand
    {

        MainEditorWindow window;
        int prevRectSize;

        public DefaultZoomCommand(MainEditorWindow window, int prevRectSize)
        {
            this.window = window;
            this.prevRectSize = prevRectSize;
        }

        public void Execute()
        {
            window.DefaultZoom();
        }

        public void Reverse()
        {
            window.SetZoom(prevRectSize);
        }


    }
}
