﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LevelEditor.Commands
{
    class FillGridCommand : ICommand
    {

        MainEditorWindow window;
        Tile[,] oldGrid;
        Image chosenImage;

        public FillGridCommand(MainEditorWindow window, Tile[,] oldGrid, Image chosenImage)
        {
            this.window = window;
            this.oldGrid = oldGrid;
            this.chosenImage = chosenImage;
        }

        public void Execute()
        {
            window.FillGrid(chosenImage);
        }

        public void Reverse()
        {
            window.SetGrid(oldGrid);
        }

    }
}
