﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace LevelEditor.Commands
{
    class DrawTileCommand : ICommand
    {
        int x, y;
        Image image;
        Image newImage;
        MainEditorWindow window;

        public DrawTileCommand(MainEditorWindow window, int x, int y, Image image, Image newImage)
        {
            this.window = window;
            this.x = x;
            this.y = y;
            this.image = image;
            this.newImage = newImage;
        }

        public void Execute()
        {
            window.DrawTile(x, y, newImage);
        }

        public void Reverse()
        {
            window.DrawTile(x, y, image);
        }
    }
}
