﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelEditor.Commands
{
    class ZoomOutCommand : ICommand
    {

        MainEditorWindow window;

        public ZoomOutCommand(MainEditorWindow window)
        {
            this.window = window;
        }


        public void Execute()
        {
            window.ZoomOut();
        }

        public void Reverse()
        {
            window.ZoomIn();
        }
    }
}
