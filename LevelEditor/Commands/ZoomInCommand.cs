﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelEditor.Commands
{
    class ZoomInCommand : ICommand
    {

        MainEditorWindow window;

        public ZoomInCommand(MainEditorWindow window)
        {
            this.window = window;
        }


        public void Execute()
        {
            window.ZoomIn();
        }

        public void Reverse()
        {
            window.ZoomOut();
        }

    }
}
