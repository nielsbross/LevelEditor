﻿namespace LevelEditor
{
    partial class MainEditorWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }



        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainEditorWindow));
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.jnsfsddfsdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testRunItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.drawGridCheckBox = new System.Windows.Forms.CheckBox();
            this.defaultZoom = new System.Windows.Forms.Button();
            this.zoomBox = new System.Windows.Forms.GroupBox();
            this.zoomOutButton = new System.Windows.Forms.Button();
            this.zoomInButton = new System.Windows.Forms.Button();
            this.fillButton = new System.Windows.Forms.Button();
            this.selectedTileBox = new System.Windows.Forms.TextBox();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.endLabel = new System.Windows.Forms.Label();
            this.playerPosLabel = new System.Windows.Forms.Label();
            this.savedLabel = new System.Windows.Forms.Label();
            this.flowLayoutgridPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.addHazardButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.editHazardButton = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.gridPanel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.javaTestItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.zoomBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.flowLayoutgridPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Items.AddRange(new object[] {
            "empty",
            "grass",
            "ground",
            "background"});
            this.comboBox1.Location = new System.Drawing.Point(13, 50);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(160, 24);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.Items.AddRange(new object[] {
            "30x30",
            "40x30",
            "50x30",
            "60x30",
            "90x30"});
            this.comboBox2.Location = new System.Drawing.Point(229, 50);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(160, 24);
            this.comboBox2.TabIndex = 2;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Tile Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(227, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Map Size";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jnsfsddfsdToolStripMenuItem,
            this.runToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(2538, 28);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // jnsfsddfsdToolStripMenuItem
            // 
            this.jnsfsddfsdToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.saveItem,
            this.saveAsItem,
            this.loadItem,
            this.exitItem});
            this.jnsfsddfsdToolStripMenuItem.Name = "jnsfsddfsdToolStripMenuItem";
            this.jnsfsddfsdToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.jnsfsddfsdToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(185, 24);
            this.saveToolStripMenuItem.Text = "New";
            // 
            // saveItem
            // 
            this.saveItem.Name = "saveItem";
            this.saveItem.Size = new System.Drawing.Size(185, 24);
            this.saveItem.Text = "Save ... CTRL + S";
            this.saveItem.Click += new System.EventHandler(this.saveItem_Click);
            // 
            // saveAsItem
            // 
            this.saveAsItem.Name = "saveAsItem";
            this.saveAsItem.Size = new System.Drawing.Size(185, 24);
            this.saveAsItem.Text = "Save As..";
            this.saveAsItem.Click += new System.EventHandler(this.saveAsItem_Click);
            // 
            // loadItem
            // 
            this.loadItem.Name = "loadItem";
            this.loadItem.Size = new System.Drawing.Size(185, 24);
            this.loadItem.Text = "Load";
            this.loadItem.Click += new System.EventHandler(this.loadItem_Click);
            // 
            // exitItem
            // 
            this.exitItem.Name = "exitItem";
            this.exitItem.Size = new System.Drawing.Size(185, 24);
            this.exitItem.Text = "Exit";
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testRunItem,
            this.javaTestItem});
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.runToolStripMenuItem.Text = "Run";
            // 
            // testRunItem
            // 
            this.testRunItem.Name = "testRunItem";
            this.testRunItem.Size = new System.Drawing.Size(175, 24);
            this.testRunItem.Text = "Test Run";
            this.testRunItem.Click += new System.EventHandler(this.testRunItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox4);
            this.groupBox1.Controls.Add(this.pictureBox3);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(513, 30);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(267, 57);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(224, 16);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(35, 33);
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Tag = "empty";
            this.pictureBox4.Click += new System.EventHandler(this.tile_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(93, 16);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(35, 33);
            this.pictureBox3.TabIndex = 11;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Tag = "grass";
            this.pictureBox3.Click += new System.EventHandler(this.tile_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(51, 16);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(35, 33);
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "ground";
            this.pictureBox2.Click += new System.EventHandler(this.tile_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(8, 16);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 33);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "background";
            this.pictureBox1.Click += new System.EventHandler(this.tile_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.drawGridCheckBox);
            this.groupBox2.Controls.Add(this.defaultZoom);
            this.groupBox2.Controls.Add(this.zoomBox);
            this.groupBox2.Controls.Add(this.fillButton);
            this.groupBox2.Location = new System.Drawing.Point(788, 30);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(429, 57);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tools";
            // 
            // drawGridCheckBox
            // 
            this.drawGridCheckBox.AutoSize = true;
            this.drawGridCheckBox.Checked = true;
            this.drawGridCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.drawGridCheckBox.Location = new System.Drawing.Point(308, 22);
            this.drawGridCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.drawGridCheckBox.Name = "drawGridCheckBox";
            this.drawGridCheckBox.Size = new System.Drawing.Size(93, 21);
            this.drawGridCheckBox.TabIndex = 21;
            this.drawGridCheckBox.Text = "Draw Grid";
            this.drawGridCheckBox.UseVisualStyleBackColor = true;
            this.drawGridCheckBox.CheckedChanged += new System.EventHandler(this.drawGridCheckBox_OnCheckedChanged);
            // 
            // defaultZoom
            // 
            this.defaultZoom.Location = new System.Drawing.Point(181, 17);
            this.defaultZoom.Margin = new System.Windows.Forms.Padding(4);
            this.defaultZoom.Name = "defaultZoom";
            this.defaultZoom.Size = new System.Drawing.Size(119, 28);
            this.defaultZoom.TabIndex = 3;
            this.defaultZoom.Text = "Default Zoom";
            this.defaultZoom.UseVisualStyleBackColor = true;
            this.defaultZoom.Click += new System.EventHandler(this.setDefaultZoom);
            // 
            // zoomBox
            // 
            this.zoomBox.Controls.Add(this.zoomOutButton);
            this.zoomBox.Controls.Add(this.zoomInButton);
            this.zoomBox.Location = new System.Drawing.Point(93, 9);
            this.zoomBox.Margin = new System.Windows.Forms.Padding(4);
            this.zoomBox.Name = "zoomBox";
            this.zoomBox.Padding = new System.Windows.Forms.Padding(4);
            this.zoomBox.Size = new System.Drawing.Size(80, 41);
            this.zoomBox.TabIndex = 2;
            this.zoomBox.TabStop = false;
            // 
            // zoomOutButton
            // 
            this.zoomOutButton.Location = new System.Drawing.Point(40, 5);
            this.zoomOutButton.Margin = new System.Windows.Forms.Padding(4);
            this.zoomOutButton.Name = "zoomOutButton";
            this.zoomOutButton.Size = new System.Drawing.Size(40, 37);
            this.zoomOutButton.TabIndex = 2;
            this.zoomOutButton.Tag = "zoomOut";
            this.zoomOutButton.Text = "-";
            this.zoomOutButton.UseVisualStyleBackColor = true;
            this.zoomOutButton.Click += new System.EventHandler(this.zoom);
            // 
            // zoomInButton
            // 
            this.zoomInButton.Location = new System.Drawing.Point(0, 5);
            this.zoomInButton.Margin = new System.Windows.Forms.Padding(4);
            this.zoomInButton.Name = "zoomInButton";
            this.zoomInButton.Size = new System.Drawing.Size(40, 37);
            this.zoomInButton.TabIndex = 1;
            this.zoomInButton.Tag = "zoomIn";
            this.zoomInButton.Text = "+";
            this.zoomInButton.UseVisualStyleBackColor = true;
            this.zoomInButton.Click += new System.EventHandler(this.zoom);
            // 
            // fillButton
            // 
            this.fillButton.Location = new System.Drawing.Point(19, 21);
            this.fillButton.Margin = new System.Windows.Forms.Padding(4);
            this.fillButton.Name = "fillButton";
            this.fillButton.Size = new System.Drawing.Size(67, 25);
            this.fillButton.TabIndex = 0;
            this.fillButton.Text = "Fill Grid";
            this.fillButton.UseVisualStyleBackColor = true;
            this.fillButton.Click += new System.EventHandler(this.fillGrid_Click);
            // 
            // selectedTileBox
            // 
            this.selectedTileBox.Location = new System.Drawing.Point(399, 50);
            this.selectedTileBox.Margin = new System.Windows.Forms.Padding(4);
            this.selectedTileBox.Name = "selectedTileBox";
            this.selectedTileBox.Size = new System.Drawing.Size(105, 22);
            this.selectedTileBox.TabIndex = 12;
            this.selectedTileBox.Tag = "selectedTile";
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // endLabel
            // 
            this.endLabel.AutoSize = true;
            this.endLabel.Location = new System.Drawing.Point(601, 949);
            this.endLabel.Name = "endLabel";
            this.endLabel.Size = new System.Drawing.Size(157, 17);
            this.endLabel.TabIndex = 19;
            this.endLabel.Text = "Level end position (0,0)";
            this.endLabel.Click += new System.EventHandler(this.endLabel_Click);
            // 
            // playerPosLabel
            // 
            this.playerPosLabel.AutoSize = true;
            this.playerPosLabel.Location = new System.Drawing.Point(433, 949);
            this.playerPosLabel.Name = "playerPosLabel";
            this.playerPosLabel.Size = new System.Drawing.Size(167, 17);
            this.playerPosLabel.TabIndex = 13;
            this.playerPosLabel.Text = "Player start position (0,0)";
            this.playerPosLabel.Click += new System.EventHandler(this.playerPosLabel_Click);
            // 
            // savedLabel
            // 
            this.savedLabel.AutoSize = true;
            this.savedLabel.Location = new System.Drawing.Point(3, 949);
            this.savedLabel.Name = "savedLabel";
            this.savedLabel.Size = new System.Drawing.Size(85, 17);
            this.savedLabel.TabIndex = 7;
            this.savedLabel.Text = "Last saved: ";
            // 
            // flowLayoutgridPanel
            // 
            this.flowLayoutgridPanel.Controls.Add(this.label3);
            this.flowLayoutgridPanel.Controls.Add(this.addHazardButton);
            this.flowLayoutgridPanel.Controls.Add(this.button1);
            this.flowLayoutgridPanel.Controls.Add(this.editHazardButton);
            this.flowLayoutgridPanel.Controls.Add(this.listBox1);
            this.flowLayoutgridPanel.Location = new System.Drawing.Point(163, 746);
            this.flowLayoutgridPanel.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutgridPanel.Name = "flowLayoutgridPanel";
            this.flowLayoutgridPanel.Size = new System.Drawing.Size(1047, 119);
            this.flowLayoutgridPanel.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "Enemies/Hazards";
            // 
            // addHazardButton
            // 
            this.addHazardButton.Location = new System.Drawing.Point(128, 2);
            this.addHazardButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addHazardButton.Name = "addHazardButton";
            this.addHazardButton.Size = new System.Drawing.Size(123, 23);
            this.addHazardButton.TabIndex = 16;
            this.addHazardButton.Text = "Add";
            this.addHazardButton.UseVisualStyleBackColor = true;
            this.addHazardButton.Click += new System.EventHandler(this.addHazardButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(257, 2);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 23);
            this.button1.TabIndex = 18;
            this.button1.Text = "Remove";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // editHazardButton
            // 
            this.editHazardButton.Location = new System.Drawing.Point(362, 2);
            this.editHazardButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.editHazardButton.Name = "editHazardButton";
            this.editHazardButton.Size = new System.Drawing.Size(101, 23);
            this.editHazardButton.TabIndex = 17;
            this.editHazardButton.Text = "Edit";
            this.editHazardButton.UseVisualStyleBackColor = true;
            this.editHazardButton.Click += new System.EventHandler(this.editHazardButton_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(3, 29);
            this.listBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(859, 68);
            this.listBox1.TabIndex = 14;
            // 
            // gridPanel
            // 
            this.gridPanel.AutoScroll = true;
            this.gridPanel.AutoScrollMinSize = new System.Drawing.Size(900, 1000);
            this.gridPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridPanel.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.gridPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gridPanel.Location = new System.Drawing.Point(163, 0);
            this.gridPanel.Margin = new System.Windows.Forms.Padding(4);
            this.gridPanel.MaximumSize = new System.Drawing.Size(2267, 0);
            this.gridPanel.MinimumSize = new System.Drawing.Size(1067, 738);
            this.gridPanel.Name = "gridPanel";
            this.gridPanel.Size = new System.Drawing.Size(2199, 738);
            this.gridPanel.TabIndex = 0;
            this.gridPanel.Scroll += new System.Windows.Forms.ScrollEventHandler(this.gridPanel_OnScroll);
            this.gridPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.gridPanel_Paint);
            this.gridPanel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gridPanel_Click);
            this.gridPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridPanel_MouseDown);
            this.gridPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridPanel_MouseMove);
            this.gridPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridPanel_MouseUp);
            this.gridPanel.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.gridPanel_onMouseWheel);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gridPanel);
            this.panel2.Controls.Add(this.playerPosLabel);
            this.panel2.Controls.Add(this.endLabel);
            this.panel2.Controls.Add(this.savedLabel);
            this.panel2.Controls.Add(this.flowLayoutgridPanel);
            this.panel2.Location = new System.Drawing.Point(17, 86);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(2521, 981);
            this.panel2.TabIndex = 20;
            // 
            // javaTestItem
            // 
            this.javaTestItem.Name = "javaTestItem";
            this.javaTestItem.Size = new System.Drawing.Size(175, 24);
            this.javaTestItem.Text = "Test Run JAVA";
            // 
            // MainEditorWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(1900, 917);
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1906, 1037);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.selectedTileBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainEditorWindow";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.form_Closed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.mainWindow_mouseWheel);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.zoomBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.flowLayoutgridPanel.ResumeLayout(false);
            this.flowLayoutgridPanel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem jnsfsddfsdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsItem;
        private System.Windows.Forms.ToolStripMenuItem loadItem;
        private System.Windows.Forms.ToolStripMenuItem exitItem;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testRunItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button fillButton;
        private System.Windows.Forms.TextBox selectedTileBox;
        private System.Windows.Forms.ToolStripMenuItem saveItem;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Button zoomInButton;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.GroupBox zoomBox;
        private System.Windows.Forms.Button zoomOutButton;
        private System.Windows.Forms.Button defaultZoom;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel gridPanel;
        private System.Windows.Forms.Label playerPosLabel;
        private System.Windows.Forms.Label endLabel;
        private System.Windows.Forms.Label savedLabel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutgridPanel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button addHazardButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button editHazardButton;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.CheckBox drawGridCheckBox;
        private System.Windows.Forms.ToolStripMenuItem javaTestItem;

        
        

    }
}

