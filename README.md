# LevelEditor
Level Editor in C#

## Feature Plan

Here we will try and keep a consistent list of the features that we feel the Level Editor needs in the future. The plan is to keep track of any feature we feel could be a good addition, but also keep an updated list of prioritised features.
At the moment the editor is written for a parallel game project found here https://github.com/stonecompass/sidescrollerlibgdx

### Priority Features
* [x] Ability to save and export level designs to a .mp file
* [ ] More tile types
* [x] Ability to choose level size at creation (or dynamically)

### General Feature Ideas
* [x] Scrolling through grid for very long levels
* [x] Loading previously created levels to re-edit
* [x] Zoom-in and out features
* [ ] Ability to choose brush sizes
* [ ] More dynamic player positioning (i.e. choose x-, y-coordinates)
* [ ] Ability to add other types of game objects (Such as enemies)




## Version

### Latest release 0.034
* Grid is now in the middle of screen
 - Dark gray background for area
 - White background for grid/canvas area
* Zoom should now work properly for any size 
 - Zoom out has occasional errors for big sizes especially
 - Fixes a bug where the buffer was too small to draw the whole grid
* Moving platforms added to hazards.
* God-mode added to game (press L3 to turn on/off)

### Release 0.033
* End point can now be set (SHIFT + CTRL + left mouse-button for placing end point)
* JAR updated
* Spike shooters get drawn as black circle with red lines indicating shooting direction.

### Release: 0.032

* Spike shooters can now be added, edited and removed from maps
* Spike shooters working in test game
* Removed bug with red colored tiles on startup

### Release: 0.031

* Scrolling works for most sizes but not for sizes bigger than the window
* Loading a file now properly works for all sizes and looks


### Release: 0.03

* Player position can be specified in the editor and read correctly by the JAR file
* Loading and editing saved .mp-files
* Test run with JAR file running the loaded file instead of the same level1.mp
* The window is now maximized

* Added game features
	* Xbox-controller support

### Release: 0.02

* Tile type choosing more dynamic (e.g. click on tile below grid instead of combo box)
* Save to text file
* Test run with JAR file
* Comboboxes now should be noneditable

### Release: 0.01

Initial project release. The editor has some very basic features for editing a simple 30x30 grid.


